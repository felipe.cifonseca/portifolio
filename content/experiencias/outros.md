---
title: "Outras Experiências Educacionais e Culturais"
draft: false
---

<div class="experiencias-container">
    <div class="experiencia">
        <h2 class = "sub">CISV</h2>
        <h3 class="exp">2014 - 2022</h3>
        <h3 class="exp">Participação dos programas da ONG CISV, organização internacional dedicada a fomentar a educação e convivência pacífica entre diferentes culturas, por meio da promoção da comunicação global.</h3>
    </div>
    <div class="experiencia">
        <h2 class = "sub">Olimpíadas</h2>
        <h3 class="exp">2016 – 2021</h3>
        <h3 class="exp"> Participação na Olimpíada Brasileira de Matemática, Olimpíada Canguru, Olimpíada Matemática Sem Fronteiras e Olimpíada Brasileira de Informática, tendo obtido medalhas de prata (2017, 2018 - Canguru) e ouro (2021, 2019, 2016 - Matemática Sem Fronteiras).</h3>
    </div>
    <div class="experiencia">
        <h2 class = "sub">Inovação social & Design</h2>
        <h3 class="exp">2018</h3>
        <h3 class="exp">Participação em atividade extracurricular de inovação social, com imersão realizada nos EUA, simultaneamente no MIT e no Olin College of Engineering, associada ao programa de imersão em inovação social do IDIN (International Development Innovation Network), voltado a aplicações da tecnologia no campo social, trabalhando fundamentos de processo criativo, design e inovação, com foco desenvolvimento de casos reais aplicados, com soluções aplicadas a comunidades vulneráveis de diversos países do mundo.</h3>
    </div>
</div>
