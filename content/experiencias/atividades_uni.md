---
title: "Atividades de Extensão Universitária"
draft: false
---

<h2 class = "sub">Iniciação Científica</h2>
<h3 class="exp">Abril 2024 – Presente</h3>
<h3 class="exp">
Iniciação Científica da Escola Politécnica da USP no Laboratório de Mecanismos, Máquinas e Robôs (LaMMaR). Projeto orientado pelo Prof. Dr. Tarcísio Antonio Hess Coelho, com o objetivo de desenvolver um robô de três dimensões com controle de trajetória para ser utilizado em aplicações práticas que podem envolver manufatura aditiva, reabilitação médica entre outras aplicações, ou uso pedagógico no departamento de mecatrônica.</h3>

<h2 class = "sub">Projeto Jupiter</h2>
<h3 class="exp">Março 2022 - Presente</h3>
<h3 class="exp">Equipe de foguetemodelismo da Escola
Politécnica da USP.</h3>
<div class="jupiter">
    <ul>
        <li>Campeão geral Latin American Space Challenge - 2022</li>
        <li>Gerente de Cargas Experimentais -2023</li>
        <li>Campeão da categoria de motores híbridos - 2023 </li>
        <li>2° lugar da categoria de 10 mil pés e 8° lugar geral Spaceport America Cup - 2024</li>
    </ul>
</div>
<h3 class="exp">Atualmente desenvolvendo um projeto para implementar um sistema de freio aerodinâmico no foguete. </h3>