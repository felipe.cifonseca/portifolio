---
title: "Competências"
draft: false
---
<div class="competencias">
    <ul>
        <li><h2 class="sub">Idiomas</h2>
            <ul class="competencias_item">
                <li class="competencias_item">Português nativo</li>
                <li class="competencias_item">Inglês Fluente
                    <ul>
                        <li>Certificado TOEFL – 103/120 (agosto 2024)</li>
                        <li>Certificado C1 Cambridge – CAE – 188/210 (dezembro de 2018)</li>
                    </ul>
                <li class="competencias_item">Alemão básico
                    <ul>
                        <li>Poliglota 100h de aula</li>
                        <li>Aulas particulares (60h horas até a presente data)</li>
                        <li>Curso intensivo a ser realizado na Alemanha em janeiro/fevereiro 2025 (75 horas) </li>
                    </ul>
                </li>
            </ul>
        </li>   
        <li><h2 class="sub">Programação</h2>
            <ul class="competencias_item">
                <li class="competencias_item">Python - Avançado</li>
                <li class="competencias_item">C++ - Intermediário</li>
                <li class="competencias_item"> Matlab/Octave – Intermediário</li>
                <li class="competencias_item">Verilog – Intermediário </li>
                <li class="competencias_item">HTML – Básico </li>
                <li class="competencias_item">CSS –  Básico </li>
            </ul>
        </li>   
        <li><h2 class="sub">Softwares</h2>
            <ul class="competencias_item">
                <li class="competencias_item">Fusion 360/ Autocad - Avançado</li>
                <li class="competencias_item">Kicad - Intermediário</li>
                <li class="competencias_item">Ansys - Intermediário</li>
                <li class="competencias_item">Excel - Básico</li>
            </ul>
        </li>   
    </ul>
</div>
